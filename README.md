# Django REST Framework Quote Management System

This project implements a RESTful API for managing quotes using Django and Django REST Framework. Users can perform CRUD (Create, Read, Update, Delete) operations on quotes, which are stored in a SQLite database.

## Installation

### Prerequisites

- Python 3.10.0
- `pip` (Python package installer)

### Setting Up

**Create a Virtual Environment**

   ```bash
   python3.10 -m venv env
   source env/bin/activate  # activate the virtual environment (Linux/Mac)
   # OR
   .\env\Scripts\activate  # activate the virtual environment (Windows)

**Clone the Repository**
git clone https://github.com/your-username/quote-management-system.git
cd quote-management-system
