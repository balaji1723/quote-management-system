from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from django.test import TestCase
from quote.models import Author, Quote

class AuthorViewSetTestCase(TestCase):
    
    def setUp(self):
        self.client = APIClient()
        self.author_data = {
            'name': 'John Doe',
            'date_of_birth': '1990-01-01'
        }
        self.author1 = Author.objects.create(name='John Doe', date_of_birth='1990-01-01')
        self.author2 = Author.objects.create(name='Jane Smith', date_of_birth='1985-05-05')

        self.valid_update_data = {'name': 'John Doe Jr.', 'date_of_birth': '1995-06-15'}
        self.invalid_update_data = {'name': '', 'date_of_birth': '1995-06-15'}


        print('self.author1 ', self.author1)
        print('self.author2 ', self.author2)

    def test_create_author(self):
        url = reverse('author-list')  # Assuming 'author-list' is the name of your viewset
        response = self.client.post(url, self.author_data, format='json')
        print('response ', response)
        # Assert that the request was successful (HTTP 201 Created)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Verify that the author was created in the database
        author = Author.objects.get(name='John Doe')
        self.assertEqual(author.name, 'John Doe')
        self.assertEqual(str(author.date_of_birth), '1990-01-01')  # Ensure date_of_birth is in the expected format

    
    def test_create_author_with_valid_data(self):
        # Test creating an author with valid data
        url = reverse('author-list')
        author_data = {
            'name': 'John Doe',
            'date_of_birth': '1990-01-01'
        }
        response = self.client.post(url, author_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    
    def test_create_author_with_missing_name(self):
        # Test creating an author with missing 'name'
        url = reverse('author-list')
        author_data = {
            'date_of_birth': '1990-01-01'
        }
        response = self.client.post(url, author_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_author_with_invalid_date_format(self):
        # Test creating an author with invalid date format
        url = reverse('author-list')
        author_data = {
            'name': 'Jane Smith',
            'date_of_birth': '01-01-1990'  # Incorrect format
        }
        response = self.client.post(url, author_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    
    def test_create_author_with_empty_data(self):
        # Test creating an author with empty data
        url = reverse('author-list')
        author_data = {}
        response = self.client.post(url, author_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_create_author_with_duplicate_name(self):
        # Test creating an author with a duplicate name
        url = reverse('author-list')
        author_data = {
            'name': 'John Doe',
            'date_of_birth': '1990-01-01'
        }
        # Create the first author with the same name
        self.client.post(url, author_data, format='json')

        # Attempt to create another author with the same name
        response = self.client.post(url, author_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_author_with_extra_fields(self):
        # Test creating an author with extra fields not defined in serializer
        url = reverse('author-list')
        author_data = {
            'name': 'John Doe',
            'date_of_birth': '1990-01-01',
            'country': 'USA'  # 'country' field is not part of the serializer
        }
        response = self.client.post(url, author_data, format='json')
        print('test_create_author_with_extra_fields ', response.status_code)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_author_with_invalid_data_type(self):
        # Test creating an author with invalid data types
        url = reverse('author-list')
        author_data = {
            'name': 123,  # Invalid data type (integer instead of string)
            'date_of_birth': '1990-01-01'
        }
        response = self.client.post(url, author_data, format='json')
        print('test_create_author_with_invalid_data_type ', response.status_code)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_author_without_date_of_birth(self):
        # Test creating an author without providing date_of_birth
        url = reverse('author-list')
        author_data = {
            'name': 'John Doe'
        }
        response = self.client.post(url, author_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_author_with_past_date_of_birth(self):
        # Test creating an author with a past date of birth
        url = reverse('author-list')
        author_data = {
            'name': 'John Doe',
            'date_of_birth': '1880-01-01'  # Assuming no authors are born before 1900
        }
        response = self.client.post(url, author_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_author_by_id(self):
        # Test retrieving an author by ID
        url = reverse('author-detail', kwargs={'pk': self.author1.pk})
        print('test_retrieve_author_by_id ', url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], 'John Doe')

    def test_retrieve_nonexistent_author(self):
        # Test retrieving a non-existent author
        url = reverse('author-detail', kwargs={'pk': 999})  # Non-existent ID
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_list_all_authors(self):
        # Test listing all authors
        url = reverse('author-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)  # Assuming there are 2 authors in the database

    def test_list_authors_with_search_filter(self):
        # Test listing authors with search filter
        url = reverse('author-list')
        response = self.client.get(url, {'search': 'John'})  # Search by name
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)  # Expecting one author with name containing 'John'

    def test_list_authors_with_invalid_search_query(self):
        # Test listing authors with invalid search query
        url = reverse('author-list')
        response = self.client.get(url, {'search': '123'})  # Invalid search query
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_list_authors_pagination(self):
        # Test pagination of author list
        url = reverse('author-list')
        response = self.client.get(url, {'page': 1, 'size': 1})  # Pagination with one author per page
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertTrue('next' in response.data)

    def test_list_authors_with_invalid_pagination_params(self):
        # Test listing authors with invalid pagination parameters
        url = reverse('author-list')
        response = self.client.get(url, {'page': 'abc', 'size': -1})  # Invalid pagination parameters
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_list_authors_ordered_by_name(self):
        # Test listing authors ordered by name
        url = reverse('author-list')
        response = self.client.get(url, {'ordering': 'name'})  # Order by name
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][0]['name'], 'Jane Smith')

    def test_list_authors_with_invalid_ordering_field(self):
        # Test listing authors with invalid ordering field
        url = reverse('author-list')
        response = self.client.get(url, {'ordering': 'invalid_field'})  # Invalid ordering field
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_update_author_valid_data(self):
        # Test updating an author with valid data
        url = reverse('author-detail', kwargs={'pk': self.author.pk})
        response = self.client.put(url, self.valid_update_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        # Verify that the author data was updated in the database
        updated_author = Author.objects.get(pk=self.author.pk)
        self.assertEqual(updated_author.name, 'John Doe Jr.')
        self.assertEqual(str(updated_author.date_of_birth), '1995-06-15')

    def test_update_author_invalid_data(self):
        # Test updating an author with invalid data
        url = reverse('author-detail', kwargs={'pk': self.author.pk})
        response = self.client.put(url, self.invalid_update_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        
        # Verify that the author data remains unchanged
        author = Author.objects.get(pk=self.author.pk)
        self.assertEqual(author.name, 'John Doe')
        self.assertEqual(str(author.date_of_birth), '1990-01-01')

    def test_update_nonexistent_author(self):
        # Test updating a non-existent author
        url = reverse('author-detail', kwargs={'pk': 999})  # Non-existent ID
        response = self.client.put(url, self.valid_update_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_partial_update_author_valid_data(self):
        # Test patching an author with valid data (partial update)
        url = reverse('author-detail', kwargs={'pk': self.author.pk})
        patch_data = {'date_of_birth': '1995-06-15'}
        response = self.client.patch(url, patch_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        # Verify that the specified field was updated in the database
        updated_author = Author.objects.get(pk=self.author.pk)
        self.assertEqual(str(updated_author.date_of_birth), '1995-06-15')

    def test_partial_update_author_invalid_data(self):
        # Test patching an author with invalid data (partial update)
        url = reverse('author-detail', kwargs={'pk': self.author.pk})
        patch_data = {'name': ''}  # Invalid update: empty name
        response = self.client.patch(url, patch_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Verify that the author data remains unchanged
        author = Author.objects.get(pk=self.author.pk)
        self.assertEqual(author.name, 'John Doe')
        self.assertEqual(str(author.date_of_birth), '1990-01-01')

    def test_partial_update_nonexistent_author(self):
        # Test patching a non-existent author
        url = reverse('author-detail', kwargs={'pk': 999})  # Non-existent ID
        patch_data = {'name': 'Jane Doe'}  # Data to patch
        response = self.client.patch(url, patch_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)



class QuoteViewSetTestCase(TestCase):
    
    def setUp(self):
        self.client = APIClient()
        self.author = Author.objects.create(name='John Doe', date_of_birth='1990-01-01')
        self.quote_data = {
            'text': 'This is a test quote.',
            'author': self.author.id,
            'source': 'Test Source',
            'creation_date': '2024-04-18T12:00:00Z'
        }

    def test_create_quote(self):
        # Test creating a new quote
        url = reverse('quote-list')
        response = self.client.post(url, self.quote_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
        # Verify that the quote was created in the database
        quote = Quote.objects.get(text='This is a test quote.')
        self.assertEqual(quote.author, self.author)
        self.assertEqual(quote.source, 'Test Source')

    def test_retrieve_quote(self):
        # Test retrieving a quote by ID
        quote = Quote.objects.create(text='Another test quote.', author=self.author)
        url = reverse('quote-detail', kwargs={'pk': quote.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['text'], 'Another test quote.')

    def test_update_quote(self):
        # Test updating an existing quote
        quote = Quote.objects.create(text='Original quote.', author=self.author)
        updated_data = {'text': 'Updated quote.', 'source': 'Updated Source'}
        url = reverse('quote-detail', kwargs={'pk': quote.id})
        response = self.client.put(url, updated_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        # Verify that the quote was updated in the database
        updated_quote = Quote.objects.get(id=quote.id)
        self.assertEqual(updated_quote.text, 'Updated quote.')
        self.assertEqual(updated_quote.source, 'Updated Source')

    def test_delete_quote(self):
        # Test deleting an existing quote
        quote = Quote.objects.create(text='Quote to delete.', author=self.author)
        url = reverse('quote-detail', kwargs={'pk': quote.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        
        # Verify that the quote was deleted from the database
        with self.assertRaises(Quote.DoesNotExist):
            Quote.objects.get(id=quote.id)