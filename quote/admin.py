from django.contrib import admin
from .models import Author, Quote

class AuthorAdmin(admin.ModelAdmin):
    list_display = ('name', 'date_of_birth')
    search_fields = ('name', 'date_of_birth')

admin.site.register(Author, AuthorAdmin)

class QuoteAdmin(admin.ModelAdmin):
    list_display = ('text', 'author', 'source', 'creation_date')
    list_filter = ('author', 'creation_date')
    search_fields = ('text', 'author__name', 'source')  # search by text, author name, or source
    raw_id_fields = ('author',)
    
admin.site.register(Quote, QuoteAdmin)
