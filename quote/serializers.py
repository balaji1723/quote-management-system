
from rest_framework import serializers
from .models import Quote, Author
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.models import User

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['id', 'name', 'date_of_birth']

class QuoteSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()

    class Meta:
        model = Quote
        fields = ['id', 'text', 'author', 'source', 'creation_date']

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)

        # Add custom data to the token response
        user = self.user
        data['user_id'] = user.id
        data['username'] = user.username
        # You can add more custom data as needed

        return data
